//
//  SettingsViewController.m
//  ScottBot
//
//  Created by Thomas Schmidt on 1/5/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import "SettingsViewController.h"
#import "ViewController.h"

@interface SettingsViewController ()
@property (strong, nonatomic) IBOutlet UITextField *urlField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;


@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _urlField.delegate = self;
    _passwordField.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    _urlField.text = [[NSUserDefaults standardUserDefaults] stringForKey:relayURLKey];
    _passwordField.text = [[NSUserDefaults standardUserDefaults] stringForKey:passwordKey];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setObject:_urlField.text forKey:relayURLKey];
    [[NSUserDefaults standardUserDefaults] setObject:_passwordField.text forKey:passwordKey];
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
