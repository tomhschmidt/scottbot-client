//
//  ViewController.h
//  ScottBot
//
//  Created by Thomas Schmidt on 1/5/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>


static NSString* const relayURLKey = @"relayURLKey";
static NSString* const passwordKey = @"passwordKey";

@interface ViewController : UIViewController

@property NSString* relayURL;
@property NSString* password;

@end

