//
//  SettingsViewController.h
//  ScottBot
//
//  Created by Thomas Schmidt on 1/5/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController<UITextFieldDelegate>

@end
