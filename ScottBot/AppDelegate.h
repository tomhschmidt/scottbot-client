//
//  AppDelegate.h
//  ScottBot
//
//  Created by Thomas Schmidt on 1/5/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSURL* relayURL;
@property (strong, nonatomic) NSString* password;


@end

