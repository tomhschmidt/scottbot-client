//
//  ViewController.m
//  ScottBot
//
//  Created by Thomas Schmidt on 1/5/15.
//  Copyright (c) 2015 Thomas Schmidt. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "ViewController.h"
#import "AFNetworking.h"

typedef enum {
    SBStateNormal,
    SBStateRequesting,
    SBStateSuccess,
    SBStateFailure
} SBState;


@interface ViewController ()
@property (strong, nonatomic) IBOutlet UILabel *statusLabel;
@property SBState state;
@property (strong, nonatomic) IBOutlet UIButton *openDoorButton;

@property UIImage* failScott;
@property UIImage* successScott;
@property UIImage* loadingScott;

@property SystemSoundID gruntSound;
@property AVAudioPlayer* gruntPlayer;

@property (strong, nonatomic) IBOutlet UIImageView *statusImageView;

@end

@implementation ViewController

@synthesize state = _state;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.state = SBStateNormal;
    if(![[NSUserDefaults standardUserDefaults] stringForKey:relayURLKey] || ![[NSUserDefaults standardUserDefaults] stringForKey:passwordKey]) {
        [[NSUserDefaults standardUserDefaults] setObject:@"http://surgeprotectorapp.com:3000" forKey:relayURLKey];
        [[NSUserDefaults standardUserDefaults] setObject:@"tecate" forKey:passwordKey];
    }
    
    _failScott = [UIImage imageNamed:@"failScott"];
    _successScott = [UIImage imageNamed:@"successScott"];
    _loadingScott = [UIImage imageNamed:@"loadingScott"];
    _gruntSound = [self loadSoundForFileName:@"grunt" ofType:@"mp3"];
    
    NSURL *path = [[NSBundle mainBundle] URLForResource:@"grunt" withExtension:@"mp3"];
    _gruntPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:path error:nil];
    [_gruntPlayer setVolume:1];
    
    UIFont* comicSans = [UIFont fontWithName:@"ComicSansMS" size:25];
    self.openDoorButton.titleLabel.font = comicSans;
    self.statusLabel.font = comicSans;
    
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:&setCategoryErr];
    [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
}

- (void)viewWillAppear:(BOOL)animated {
    self.relayURL = [[NSUserDefaults standardUserDefaults] stringForKey:relayURLKey];
    self.password = [[NSUserDefaults standardUserDefaults] stringForKey:passwordKey];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setState:(SBState)state {
    _state = state;
    //AudioServicesPlaySystemSound(_gruntSound);
    if([_gruntPlayer isPlaying]) {
        [_gruntPlayer stop];
        [_gruntPlayer setCurrentTime:0];
    }
    
    [_gruntPlayer play];
    switch(state) {
        case SBStateNormal:
            self.statusLabel.hidden = YES;
            self.openDoorButton.enabled = YES;
            break;
        case SBStateRequesting:
            self.statusLabel.hidden = YES;
            self.openDoorButton.enabled = NO;
            self.statusLabel.hidden = NO;
            self.statusLabel.text = @"Opening door...";
            self.statusLabel.textColor = [UIColor blackColor];
            [self setStatusImage:_loadingScott];
            break;
        case SBStateSuccess:
            self.openDoorButton.enabled = YES;
            self.statusLabel.hidden = NO;
            self.statusLabel.text = @"Door opened!";
            self.statusLabel.textColor = [UIColor greenColor];
            [self setStatusImage:_successScott];
            break;
        case SBStateFailure:
            self.openDoorButton.enabled = YES;
            self.statusLabel.hidden = NO;
            //self.statusLabel.text = @"Failed to open door";
            self.statusLabel.textColor = [UIColor redColor];
            [self setStatusImage:_failScott];
            break;
    }
}

- (SystemSoundID)loadSoundForFileName:(NSString*)fileName ofType:(NSString*)type {
    SystemSoundID sound;
    NSString *path = [[NSBundle mainBundle]
                      pathForResource:fileName ofType:type];
    NSURL *url = [NSURL fileURLWithPath:path];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)url, &sound);
    return sound;
}

- (void)setStatusImage:(UIImage*)image {
//    CGRect rect = self.statusImageView.frame;
//    rect.size.width *= (rect.size.height / image.size.height);
//    self.statusImageView.image = image;
//    self.statusImageView.frame = rect;
    self.statusImageView.image = image;
    [self.view setNeedsLayout];
}

- (void)viewDidLayoutSubviews {
    CGRect rect = self.statusImageView.frame;
    if(self.statusImageView.image) {
        rect.size.width *= (rect.size.height / self.statusImageView.image.size.height);
        rect.origin.x = (self.view.frame.size.width / 2) - (rect.size.width / 2);
        self.statusImageView.frame = rect;
    }
}

- (SBState)state {
    return _state;
}

- (IBAction)openDoorButtonPressed:(id)sender {
    [self setState:SBStateRequesting];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:self.relayURL parameters:@{ @"password" : self.password} success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
            [self setState:SBStateSuccess];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        self.statusLabel.text = error.localizedDescription;
        [self setState:SBStateFailure];
    }];
}

- (void)requestFailed {
    
}

- (IBAction)infoButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"segueToSettingsViewController" sender:self];
}
@end
